require 'dm-rails/middleware/identity_map'
class ApplicationController < ActionController::Base
  use Rails::DataMapper::Middleware::IdentityMap
  #protect_from_forgery

  def authenticated_coach?
    if (current_user.nil? || current_user.role == :customer )
      error_message = I18n.t("controllers.application_controller.access_denied")
      respond_to do |format|        
        format.any { render :status => 401, :json => '{ "errors" : [ "'+ error_message +'" ] }' }        
      end
      return false
    end
    true
  end

  def authenticated_user?    
    if (current_user.nil?)
      error_message = I18n.t("controllers.application_controller.access_denied")
      respond_to do |format|        
        Rails.logger.error "###################### ApplicationController error_message = #{error_message}"
        format.any { render :status => 401, :json => '{ "errors" : [ "'+error_message+'" ] }' }        
      end
      return false
    end
    true
  end

end
