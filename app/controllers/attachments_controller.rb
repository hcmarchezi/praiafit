class AttachmentsController < ApplicationController

  before_filter :authenticated_user?

  def download
    filename = params["filename"].to_s
    message_id = params["message_id"].to_s
    extension = File.extname(filename)  ##params["format"].to_s
    user_id = current_user.id.to_s

    Rails.logger.error "########### Attachments::download params = #{params}"
    Rails.logger.error "############ filename=" + filename + " message_id=" + message_id + " extension=" + extension + " user_id=" + user_id

    if current_user.role == :customer
      messages = CoachMessage.all(:id => message_id, :from_id => user_id) + CoachMessage.all(:id => message_id, :to_id => user_id)
    else
      messages = CoachMessage.all(:id => message_id)
    end

    Rails.logger.error "############ messages=" + messages.to_a.to_s

    message = messages.first
    file_path   = "#{Rails.root}#{APP_CONFIG['file_path']}#{message_id}/#{filename}" 
    Rails.logger.error "########### file_path = #{file_path}"
    file_exists = File.exists?(file_path)

    Rails.logger.error "########### message = " + message.to_s + " file_exists = " + file_exists.to_s;

    if ( message.nil? ) || ( !file_exists )
      render :status => 401, :json => { "errors" => [ I18n.t("controllers.attachments_controller.unauthorized_inexistent_attachment") ] } 
    else
      send_file(file_path, filename: "#{filename}", type: "application/#{extension}")
    end
  end
end
