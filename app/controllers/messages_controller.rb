class MessagesController < ApplicationController

  before_filter :authenticated_user?

  # GET /messages
  # GET /messages.json
  def index
    @messages = []
    if current_user.role == :customer
      @messages = Message.all( :from_id => current_user.id ).to_a + CoachMessage.all(:to_id => current_user.id ).to_a
    elsif current_user.role == :coach
      @messages = Message.all.to_a
    end
    @messages = @messages.sort { |m1,m2| m1.date <=> m2.date }

    respond_to do |format|
      format.html # index.html.erb
      format.json # { render json: @messages }
    end
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    @message = Message.get(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @message }
    end
  end

  # GET /messages/new
  # GET /messages/new.json
  def new
    @message = Message.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @message }
    end
  end

  # GET /messages/1/edit
  def edit
    @message = Message.get(params[:id])
  end

  # POST /messages
  # POST /messages.json
  def create
    if current_user.role == :customer
      @message = Message.new(params[:message])      
    else
      @message = CoachMessage.new(params[:message])      
    end
    @message.date = Time.now
    @message.from = current_user

    respond_to do |format|
      if @message.save
        files = extract_uploaded_files(params)      
        attachments = [ ]
        files.each{ |file|
          attachment = Attachment.new
          attachment.message = @message
          attachment.filename = file.original_filename
          attachment.save_uploaded_file(file)
          attachment.save
          attachments.push(attachment)
        }
        format.html { redirect_to "/" }
        format.json { render "create", status: :created }   # { render json: @message, status: :created, location: @message }
      else
        format.html { render action: "new" }
        format.json { render json: @message.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  def extract_uploaded_files(params)
    files = Array.new
    params.keys.each{ |key|
      if params[key].class.name == "ActionDispatch::Http::UploadedFile"
        files.push(params[key])
      end
    }     
    files
  end

  # PUT /messages/1
  # PUT /messages/1.json
  def update
    @message = Message.get(params[:id])

    respond_to do |format|
      if @message.update(params[:message])
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message = Message.get(params[:id])
    @message.destroy

    respond_to do |format|
      format.html { redirect_to messages_url }
      format.json { head :no_content }
    end
  end

  

end
