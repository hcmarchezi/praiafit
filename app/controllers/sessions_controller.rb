class SessionsController < Devise::SessionsController

  # POST /resource/sign_in
  def create
    Rails.logger.error "######### BEFORE ##################### cookies=#{cookies.to_json}"
    Rails.logger.error "######### BEFORE ##################### session=#{session}"
       
    self.resource = warden.authenticate(auth_options)
    
    if (resource.nil?)
      self.resource = User.new
      self.resource.errors.add(:base, I18n.t("devise.failure.invalid"))
    else            
      set_flash_message(:notice, :signed_in)
      sign_in(resource_name, self.resource)    
    end

    Rails.logger.error "########## AFTER #################### cookies=#{cookies.to_json}"
    Rails.logger.error "########## AFTER #################### session=#{session}"

    respond_to do |format|
      format.html { respond_with resource, :location => after_sign_in_path_for(resource) }
      format.json { @resource = self.resource }
    end

  end

end
