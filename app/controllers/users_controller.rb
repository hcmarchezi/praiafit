class UsersController < ApplicationController

  #before_filter :check_authenticated_coach

  # GET /users
  # GET /users.json
  def index
    if authenticated_coach?
      @users = User.all( :order => [ :name ])
      respond_to do |format|
        format.html # index.html.erb
        format.json #{ render json: @users }
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    if authenticated_coach?
      @user = User.get(params[:id])
      respond_to do |format|
        if @user.nil?
          format.json { render json: { "errors" => [ I18n.t("controllers.users_controller.inexistent_user") ] } }
        else
          format.html # show.html.erb
          format.json 
        end
      end
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.get(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    if authenticated_coach?      
      params["user"]["role"] = Role.to_symbol( params["user"]["role"] )
      params["user"]["password_confirmation"] = params["user"]["password"]
      @user = User.new(params[:user])
      respond_to do |format|
        if @user.save
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render "create", status: :created }
        else
          format.html { render action: "new" }
          format.json { render json: @user.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    if authenticated_coach?      
      @user = User.get(params[:id])
      params["user"]["role"] = Role.to_symbol( params["user"]["role"] )
      respond_to do |format|
        if @user.nil?
          format.json { render json: { "errors" => [ I18n.t("controllers.users_controller.inexistent_user") ] } }
        elsif params[:user].nil?
          format.json { render json: { "errors" => [ I18n.t("controllers.users_controller.empty_user_data") ] } }
        elsif @user.update(params[:user])
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json 
        else
          format.html { render action: "edit" }
          format.json { render json: @user.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if authenticated_coach?
      @user = User.get(params[:id])
      @user.destroy
      Message.all(:from_id => params[:id]).destroy
      CoachMessage.all(:to_id => params[:id]).destroy
      respond_to do |format|
        format.html { redirect_to users_url }
        format.json { head :no_content }
      end
    end
  end

  # GET /admin/users/1/messages
  def messages
    if authenticated_coach?
      user = User.get(params[:id])
      @messages = Message.all( :from_id => user.id ).to_a + CoachMessage.all(:to_id => user.id ).to_a
      @messages = @messages.sort { |m1,m2| m1.date <=> m2.date }
      respond_to do |format|      
        format.json 
      end
    end
  end

  def profile
    if authenticated_user?
      respond_to do |format|
        if current_user.update(params[:user]) 
          format.json
        else
          format.json { render json: current_user.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end
  end

  def whoami
    @user = current_user
    @authenticated = !@user.nil?
  end

end
