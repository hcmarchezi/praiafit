class Attachment
  include DataMapper::Resource
  
  property :id, Serial

  belongs_to :message, "CoachMessage", :required => false
  validates_presence_of :message, :message => I18n.t("models.attachment.blank_message")

  property :filename, String, :required => true, 
    :messages => { 
      :presence => I18n.t("models.attachment.blank_filename") 
  } 

  def save_uploaded_file(file)
    file_path = "#{Rails.root}#{APP_CONFIG['file_path']}#{self.message.id}/#{self.filename}"

    if File.directory?("#{Rails.root}#{APP_CONFIG['file_path']}") == false
      Dir.mkdir("#{Rails.root}#{APP_CONFIG['file_path']}")     
    end

    if File.directory?("#{Rails.root}#{APP_CONFIG['file_path']}#{self.message.id}") == false
      Dir.mkdir("#{Rails.root}#{APP_CONFIG['file_path']}#{self.message.id}")     
    end

    File.open(file_path,"wb") { |outfile|
      outfile.write file.tempfile.read
    }    
  end

end
