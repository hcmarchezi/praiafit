class CoachMessage < Message
  belongs_to :to, User, :required => false
  validates_presence_of :to, :message => I18n.t("models.coach_message.blank_to")

  before :save, :check_origin_and_destination

  def check_origin_and_destination
    from_user = self.from
    to_user = self.to

    if (from_user.role == to_user.role)
      role = from_user.role
      if (role == :customer)
        return self.errors[:base] << I18n.t("models.message.customer_to_customer")
      else
        return self.errors[:base] << I18n.t("models.message.coach_to_coach")
      end
    end 

  end

end
