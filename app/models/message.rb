class Message
  include DataMapper::Resource

  property :id, Serial
  property :type, Discriminator # support for inheritance

  property :date, DateTime, :required => true, :messages => { :presence  => I18n.t("models.message.blank_date") }
  property :content, Text, :required => true, :messages => { :presence  => I18n.t("models.message.blank_content") }
  belongs_to :from, User, :required => false
  validates_presence_of :from, :message => I18n.t("models.message.blank_from")

  has n, :attachments

  def position
    pos = ""
    if from.role == :customer
      pos = "left"
    else
      pos = "right"
    end
  end

end
