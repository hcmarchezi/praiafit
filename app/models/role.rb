class Role

  def self.to_string( role_symbol )
    I18n.t("models.role.#{role_symbol}")
  end

  def self.to_symbol( role_string )
    I18n.t("models.role.#{role_string}").to_sym
  end

end
