class StringHelper

  def self.convert_special_characters(sentence)
    sentence = sentence.gsub("\n","\\n")
    sentence = sentence.gsub("\r","\\r")
    sentence
  end

end
