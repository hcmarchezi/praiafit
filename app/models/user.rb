class User

  include DataMapper::Resource
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  # :registerable, 
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  property :email, String, :required => true, :length => 100, :unique => true, :format => :email_address,
        :messages => { 
                :presence  => I18n.t("models.user.blank_email"),
                :is_unique => I18n.t("models.user.not_unique_email"),
                :format    => I18n.t("models.user.email_format")
        } 
  property :encrypted_password, String, :required => true,  :length => 255, :messages => { :presence => I18n.t("models.user.blank_encrypted_password") }

  ## Recoverable
  property :reset_password_token,   String
  property :reset_password_sent_at, DateTime

  ## Rememberable
  property :remember_created_at, DateTime

  ## Trackable
  property :sign_in_count,      Integer, :default => 0
  property :current_sign_in_at, DateTime
  property :last_sign_in_at,    DateTime
  property :current_sign_in_ip, String
  property :last_sign_in_ip,    String

  ## Encryptable
  # property :password_salt, String

  ## Confirmable
  # property :confirmation_token,   String
  # property :confirmed_at,         DateTime
  # property :confirmation_sent_at, DateTime
  # property :unconfirmed_email,    String # Only if using reconfirmable

  ## Lockable
  # property :failed_attempts, Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # property :unlock_token,    String # Only if unlock strategy is :email or :both
  # property :locked_at,       DateTime

  ## Token authenticatable
  # property :authentication_token, String, :length => 255

  ## Invitable
  # property :invitation_token, String, :length => 255

  property :id, Serial
  property :name, String, :required => true, :length => 120, :messages => { :presence => I18n.t("models.user.blank_name") }
  validates_length_of :name, :within => 5..120, :message => I18n.t("models.user.name_size")
  
  #belongs_to :role, :required => false 
  #validates_presence_of :role, :message => I18n.t("models.user.blank_role")

  property :role, Enum[ :customer, :coach ], :required => true, :messages => { :presence => I18n.t("models.user.blank_role") }


  after :valid?, :sanitize_messages

  # Due to a DataMapper / Devise bug I need to eliminate extra default error messages
  # TODO: Find a better way to fix this bug
  def sanitize_messages
    if (self.errors.size > 0)
      self.errors[:email].delete("Email can't be blank")
      self.errors[:email].delete("Email is invalid")
      self.errors[:password].delete("Password can't be blank")
      self.errors[:email].delete("Email has already been taken")
    end
  end

  def self.customers
#    customer_role = Role.first(:name => "Customer")
    users = User.all( :role => :customer ).to_a
    users
  end

  def self.coaches
#    coach_role = Role.first(:name => "Coach")
    users = User.all( :role => :coach ).to_a
    users
  end

  def features
    features_array = [ "messaging", "profile" ]
    if self.role == :coach
      features_array.push("user management")
    end
    features_array
  end
  
end
