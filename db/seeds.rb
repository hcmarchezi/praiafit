# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

########## PRODUCTION

admin = User.create(
  :name => "admin", 
  :email => "hcmarchezi@gmail.com", 
  :password => "doisirmaos", 
  :password_confirmation => "doisirmaos",
  :role => :coach
)

puts "######## admin.errors = #{admin.errors.to_a}"

jptrainner = User.create(
  :name => "João Paulo Cardoso Marchezi", 
  :email => "jptrainner@yahoo.com.br", 
  :password => "doisirmaos", 
  :password_confirmation => "doisirmaos",
  :role => :coach
)

puts "######## jptrainner.errors = #{jptrainner.errors.to_a}"

########## DEVELOPMENT, TEST

=begin

User.create(
  :name => "admin", 
  :email => "admin@admin.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :coach
)

@coach1 = User.create(
  :name => "João Paulo Cardoso Marchezi", 
  :email => "jptrainner@gmail.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :coach
)

@coach2 = User.create(
  :name => "Wagner de Souza Lopes", 
  :email => "wagner@gmail.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :coach
)

@customer1 = User.create(
  :name => "José Carlos Araújo", 
  :email => "jose@email.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :customer
)

@customer2 = User.create(
  :name => "Melinda Gates", 
  :email => "melinda@email.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :customer
)

@customer3 = User.create(
  :name => "Ricardo Magalhães", 
  :email => "chupetinha@gmail.com", 
  :password => "12345678", 
  :password_confirmation => "12345678",
  :role => :customer
)

puts "################ @coach1 = #{@coach1.to_json}"
puts "################ @coach2 = #{@coach2.to_json}"
puts "################ @customer1 = #{@customer1.to_json}"
puts "################ @customer2 = #{@customer2.to_json}"
puts "################ @customer3 = #{@customer3.to_json}"

@msg = CoachMessage.create(
  :from => @coach1,
  :to => @customer1,
  :content => "Mensagem randomica do coach1 para customer1",
  :date => Time.new(2013,6,20, 9, 10,15) 
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer1,
  :content => "Mensagem randomica do coach1 para customer1",
  :date => Time.new(2013,6,20,10,10,15)
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer1,
  :content => "MenWhatever frekfbrekfbrwkfbrkbf fkrbfkrwfb 1",
  :date => Time.new(2013,6,20,19,10,15)
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer2,
  :content => "Mensrhfbrjhfvbrhjvbrhjbvrbf rfrwhf bwkfwe bfk omer1",
  :date => Time.new(2013,06,20,20,20,25)
)

CoachMessage.create(
  :from => @coach2,
  :to => @customer3,
  :content => "Men 0000000000000000000000000000000000000000000 omer1",
  :date => Time.new(2013,06,20,21,11,15)
)

CoachMessage.create(
  :from => @coach2,
  :to => @customer2,
  :content => "Menfrfbrkhfbrh ffkrbfrkfhb fkrf brkhw] sagem randomica do coach1 para customer1",
  :date => Time.new(2013,6,20,22,30,15)
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer3,
  :content => "Mensa TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT stomer1",
  :date => Time.new(2013,6,21,9,10,15)
)

CoachMessage.create(
  :from => @coach2,
  :to => @customer1,
  :content => "Mensagem FFFFFFFFFFFFFFFFFFFFFf para customer1",
  :date => Time.new(2013,6,21,9,30,15)
)

CoachMessage.create(
  :from => @coach2,
  :to => @customer3,
  :content => "Mens frjhbfrhjbf fejhbrehbrehjb rejhb rjhb recustomer1",
  :date => Time.new(2013,6,21,9,45,15)
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer3,
  :content => "Mensação ção ção ção áááéééééééé    tomer1",
  :date => Time.new(2013,6,21,10,10,15)
)

CoachMessage.create(
  :from => @coach1,
  :to => @customer1,
  :content => "Mens 3333333333333333333333333333333333333 er1",
  :date => Time.new(2013,6,21,13,10,15)
)

###############

@cmsg = Message.create(
  :from => @customer1,
 # :to => @coach1,
  :content => "Mensagem randomica do coach1 para customer1",
  :date => Time.new(2013,06,20,9,10,15)
)

Message.create(
  :from => @customer1,
#  :to => @coach1,
  :content => "Men AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA r1",
  :date => Time.new(2013,6,20,10,10,15)
)

Message.create(
  :from => @customer1,
#  :to => @coach1,
  :content => "Mens BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBbb r1",
  :date => Time.new(2013,6,20,11,11,11)
)

Message.create(
  :from => @customer2,
#  :to => @coach1,
  :content => "Men CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCc r1",
  :date => Time.new(2013,6,20,15,10,15)
)

Message.create(
  :from => @customer2,
#  :to => @coach1,
  :content => "Mensf FFFFFFFFFFFFFFFFFFFFFFF éíóó",
  :date => Time.new(2013,06,21,9,20,15)
)

Message.create(
  :from => @customer2,
#  :to => @coach1,
  :content => "Mensag MMMMMMMMMMMMMMMMMMMMMMMMMMMM omer1",
  :date => Time.new(2013,6,21,13,10,15)
)

Message.create(
  :from => @customer1,
#  :to => @coach1,
  :content => "Cerca de 7.000 pessoas estão concentradas na praça Sete, no centro de Belo Horizonte, para uma marcha rumo ao estádio do Mineirão na tarde deste sábado, segundo a Polícia Militar.",
  :date => Time.new(2013,6,22,9,0,15)
)

Message.create(
  :from => @customer3,
#  :to => @coach1,
  :content => "O grupo leva cartazes com críticas a políticos e diferentes reivindicações. Não há bandeiras de partidos políticos.",
  :date => Time.new(2013,6,20,9,0,5)
)

Message.create(
  :from => @customer3,
#  :to => @coach1,
  :content => "Da igreja São Francisco, na Pampulha, também sairá uma manifestação de professores rumo ao estádio.",
  :date => Time.new(2013,6,20,12,0,0)
)

Message.create(
  :from => @customer3,
#  :to => @coach1,
  :content => "No centro da cidade ainda há muita gente dispersa, que deve engrossar a marcha até o Mineirão, que nesta tarde recebe o jogo da Copa das Confederações entre Japão e México.",
  :date => Time.new(2013,6,20,15,5,15)
)

=end
