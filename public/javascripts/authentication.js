PraiaFit.Auth = { };

//////////////////////////////////
// Check if some user is authenticated
// If positive, it returns the user name, email and role
// params: JSON object
//  {
//    responseFn : function(data) {...}
//  }
PraiaFit.Auth.whoAmI = function(params)
{
  PraiaFit.Core.ajaxJSONGet({
    url: "/users/whoami",
    responseFn : params.responseFn
  });
};


//////////////////////////////////
// User sign in
// params: JSON Object
//  {
//    email : string
//    password: string
//    responseFn : function(data) { ........ }
//  }
PraiaFit.Auth.signIn = function(params)
{
  PraiaFit.Core.ajaxJSONPost({ 
    url: "/users/sign_in",
    post_data: "user[email]="+params.email+"&user[password]="+params.password,
    responseFn : params.responseFn
  });
};

///////////////////////////////////
// User sign out
// params: JSON Object
//   {   
//     responseFn : function(data,status) { ............. }
//   }
PraiaFit.Auth.signOut = function(params)
{
  PraiaFit.Core.ajaxJSONDelete({
    url: "/users/sign_out",
    responseFn : params.responseFn
  });
};
     

