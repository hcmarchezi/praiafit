var PraiaFit = { };

PraiaFit.Core = { };

PraiaFit.Core.requestErrorHandle = function(jqxhr,status,error)
{
  console.log( "jqxhr="+JSON.stringify(jqxhr)+" error:"+error+" status:"+status );
}

/////////////////////////////////////////////
// Ajax JSON Post request
// params : JSON Object
// {
//   url: string, 
//   post_data: string,
//   responseFn: function(data) { .... },
//   errorFn : function(jqxhr,status,error) { .... }
// }
PraiaFit.Core.ajaxJSONPost = function(params)
{
  $.ajax
  ({
    url: params.url,
    type: "POST",
    data: params.post_data,
    dataType: "json",
    success: params.responseFn,
    error: params.errorFn
  });  
};

/////////////////////////////////////////////
// Ajax JSON Get request
// params : JSON Object
// {
//   url: string, 
//   query_params: string, (optional)
//   responseFn: function(data) { .... }
// }
PraiaFit.Core.ajaxJSONGet = function(params)
{
  if (params.query_params == undefined)
  {
    params.query_params = "" 
  }  
  else
  {
    params.query_params = "?" + params.query_params
  }

  console.log("GET " + params.url + params.query_params);

  $.ajax
  ({
    url: params.url + params.query_params,
    type: "GET",
    dataType: "json",
    success: params.responseFn,
    error: PraiaFit.Core.requestErrorHandle
  });  
};

/////////////////////////////////////////////
// Ajax JSON Put request
// params : JSON Object
// {
//   url: string, 
//   post_data: string,
//   responseFn: function(data) { .... },
//   errorFn : function(jqxhr,status,error) { .... }
// }
PraiaFit.Core.ajaxJSONPut = function(params)
{
  $.ajax
  ({
    url: params.url,
    type: "PUT",
    data: params.post_data,
    dataType: "json",
    success: params.responseFn,
    error: params.errorFn
  });  
};

/////////////////////////////////////////////
// Ajax JSON Delete request
// params : JSON Object
// {
//   url: string, 
//   query_params: string, (optional)
//   responseFn: function(data) { .... }
// }
PraiaFit.Core.ajaxJSONDelete = function(params)
{
  if (params.query_params == undefined)
  {
    params.query_params = "" 
  }  
  else
  {
    params.query_params = "?" + params.query_params
  }

  $.ajax
  ({
    url: params.url + params.query_params,
    type: "DELETE",
    dataType: "json",
    success: params.responseFn,
    error: PraiaFit.Core.requestErrorHandle
  });  
};

///////////////////////////////////////////
// Gets an HTML element and makes it 
// visible or hidden 
// params: JSON Object
//  {
//    elementId : String => HTML element id,
//    visible : Boolean => true is visible, false is hidden
//  }
PraiaFit.Core.display = function(elementId,visible)
{
  var element = document.getElementById(elementId);
  if (element == undefined)
  {
    console.log("ERROR: PraiaFit.Core.display - element not found: " + elementId);
    return;
  } 
    
  if (visible)
  {
    var visiblePosition = element.className.indexOf(" visible ");
    if (visiblePosition == -1)
    {
      element.className += " visible ";
    }    
  }
  else
  {
    element.className = element.className.replace(" visible "," ");
  }  

}



    
