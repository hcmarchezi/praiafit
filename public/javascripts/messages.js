PraiaFit.Messages = { };

///////////////////////////////////////////
// List user's messages order by date
// params : Object
//  {
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Messages.list = function(params) {
  PraiaFit.Core.ajaxJSONGet({
    url: "/messages",
    responseFn : params.responseFn
  });
};

//////////////////////////////////////////
// Create a message
// params : Object
//  {
//    to_id : Integer - destination user id
//    content : String - message text content
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Messages.send = function(params) {

  var post_data = "";
  post_data = "message[content]="+params.content;
  if (params.to_id != undefined)
  {
    post_data += "&message[to_id]="+params.to_id;
  }

  PraiaFit.Core.ajaxJSONPost({
    url: "/messages",
    post_data: post_data,
    responseFn : params.responseFn,
    errorFn : params.errorFn
  });
};

