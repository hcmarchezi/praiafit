
window.onload = function() {

/*
  ONDE PAREI !!!
  * Test JavaScript user removal
  * Implement user inclusion !!!
  * Test user role update
  * Implement profile update
  * Publish for JP tests

  IMPROVEMENTS
  * Test negative cases for messages, users and profile
  * Refine error handling in the system ( bootstrap lightbox for errors )
  * Change background
*/
  

  // Shows login area if no user is authenticated or desktop otherwise
  PraiaFit.Auth.whoAmI({
    responseFn : function(obj) {
      if (obj["authenticated"]) 
      {
        PraiaFit.UI.currentUser = obj;
        console.log("PraiaFit.UI.currentUser="+JSON.stringify(PraiaFit.UI.currentUser));

        PraiaFit.Core.display("praiafit-authenticated",true);
        PraiaFit.Core.display("praiafit-login",false);
        PraiaFit.UI.Core.setAuthenticatedWallpaper();

        PraiaFit.UI.setWelcomeMessageForUser("praiafit-welcome-user",obj["name"]);
        PraiaFit.UI.displayUsersMenuItemIfCoach("praiafit-navbar",obj);
        PraiaFit.UI.includeUsersDropdownIfCoach("praiafit-message-form-to-dropdown",obj);
        PraiaFit.UI.includeMessageAttachmentsIfCoach("praiafit-message-form-attachments",obj);

        PraiaFit.Core.display("praiafit-messages",true);
        PraiaFit.UI.executeMessagesMenuItemAction();
        PraiaFit.UI.setProfileData(obj);

        var messagesMenuItem = document.getElementById("menu-item-messages-link");
        messagesMenuItem.onclick = function() { PraiaFit.UI.executeMessagesMenuItemAction(); };

        if (obj["role"]=="coach")
        {
          var usersMenuItem = document.getElementById("menu-item-users-link");
          usersMenuItem.onclick = function() { PraiaFit.UI.executeUsersMenuItemAction(); };
        }

        var profileMenuItem = document.getElementById("menu-item-profile-link");
        profileMenuItem.onclick = function() { PraiaFit.UI.executeProfileMenuItemAction(); };

        var aboutMenuItem = document.getElementById("menu-item-about-link");
        aboutMenuItem.onclick = function() { PraiaFit.UI.executeAboutMenuItemAction(); };

        var logoutButton = document.getElementById("praiafit-logout");
        logoutButton.onclick = function() {  
          PraiaFit.UI.logoutAction(); 
        };

      } 
      else 
      {
        PraiaFit.UI.currentUser = obj;
        PraiaFit.Core.display("praiafit-authenticated",false);
        PraiaFit.Core.display("praiafit-login",true);
        PraiaFit.UI.Core.setLoginWallpaper();
      }       
    }
  });

  var loginButton = document.getElementById("login-submit");
  loginButton.onclick = function() { 
    PraiaFit.UI.loginAction("login-email","login-password","login-errors"); 
  };
 


/*
  var messageSendButton = document.getElementById("praiafit-message-form-send-button");
  messageSendButton.onclick = function() { 
    PraiaFit.UI.sendMessageAction("praiafit-message-form-to-select","praiafit-message-form-content-textarea"); 
  }
*/

  var userCreateButton = document.getElementById("praiafit-user-form-create-button");
  userCreateButton.onclick = function() { 
    PraiaFit.UI.createUserAction({
      nameInputElementId  :    "praiafit-user-form-name-input",
      emailInputElementId :    "praiafit-user-form-email-input",
      roleSelectElementId :    "praiafit-user-form-role-select",
      passwordInputElementId : "praiafit-user-form-password-input"
    });
  };

  var profileUpdateButton = document.getElementById("profile-form-button");
  profileUpdateButton.onclick = function() {
    PraiaFit.UI.editProfileButtonAction("profile-form-email","profile-form-button");
  };

};
