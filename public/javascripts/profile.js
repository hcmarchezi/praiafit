PraiaFit.Profile = { };

///////////////////////////////////////////
// Update authenticated user's profile
// params: Object
//  {
//    email : String,
//    responseFn : function(data) { .... },
//    errorFn : function(jqxhr,status,error) { .... }
//  }
PraiaFit.Profile.update = function(params) {

  post_data = "user[email]="+params.email;

  PraiaFit.Core.ajaxJSONPut({
    url: "/users/profile",
    post_data : post_data,
    responseFn : params.responseFn,
    errorFn : params.errorFn
  });
};
