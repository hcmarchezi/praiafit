PraiaFit.UI.setWelcomeMessageForUser = function(elementId,userName)
{
  welcomeUserArea = document.getElementById(elementId);
  welcomeUserArea.innerHTML = "";
  welcomeUserArea.innerHTML += "<small>Bem vindo, " + userName + "</small>  ";
  welcomeUserArea.innerHTML += "<button id=\"praiafit-logout\" class=\"btn\">Sair</button>";
};

PraiaFit.UI.setProfileData = function(obj)
{
  var profileName  = document.getElementById("profile-form-name");
  profileName.value = obj["name"];
  var profileEmail = document.getElementById("profile-form-email");
  profileEmail.value = obj["email"]; 
};

PraiaFit.UI.displayUsersMenuItemIfCoach = function(elementId,obj)
{
  if (obj["role"]=="coach")
  {
    PraiaFit.UI.Core.insertUsersMenuItem("praiafit-navbar");
  }
};

PraiaFit.UI.includeUsersDropdownIfCoach = function(dropdownDivElementId,obj)
{
  if (obj["role"]=="coach")
  {
    var dropdownDiv = document.getElementById(dropdownDivElementId);
    dropdownDiv.innerHTML = "";
    dropdownDiv.innerHTML += "<div class = \"control-group\" id=\"praiafit-message-form-to-dropdown\">";
    dropdownDiv.innerHTML += "  <label class=\"control-label\" id=\"praiafit-message-form-to-label\">Para:</label>";
    dropdownDiv.innerHTML += "  <div class=\"controls\">";
    dropdownDiv.innerHTML += "    <select id=\"praiafit-message-form-to-select\" name=\"message[to_id]\" style=\"width:95%;\" >";
    dropdownDiv.innerHTML += "    </select>";
    dropdownDiv.innerHTML += "  </div>";
    dropdownDiv.innerHTML += "</div>";
    dropdownDiv.innerHTML += "";
  }
};

PraiaFit.UI.includeMessageAttachmentsIfCoach = function(attachmentsDivElementId,obj)
{
  if (obj["role"]=="coach")
  {
    var attachmentsDiv = document.getElementById(attachmentsDivElementId);
    attachmentsDiv.innerHTML = "";
    attachmentsDiv.innerHTML += "<label class=\"control-label\">Anexos:</label>";
    attachmentsDiv.innerHTML += "<div class=\"controls\">";
    attachmentsDiv.innerHTML += "  <input type=\"file\" name=\"attachment1\" style=\"width:30%;\" />";
    attachmentsDiv.innerHTML += "  <input type=\"file\" name=\"attachment2\" style=\"width:30%;\" />";
    attachmentsDiv.innerHTML += "  <input type=\"file\" name=\"attachment3\" style=\"width:30%;\" />";
    attachmentsDiv.innerHTML += "</div>";
  }
};

PraiaFit.UI.executeMessagesMenuItemAction = function()
{
  PraiaFit.Core.display("praiafit-messages",true);
  PraiaFit.Core.display("praiafit-users",false);
  PraiaFit.Core.display("praiafit-profile",false);
  PraiaFit.Core.display("praiafit-about",false);
  document.getElementById("menu-item-messages").className = "active";
  document.getElementById("menu-item-profile").className = "";
  document.getElementById("menu-item-about").className = "";
  if (PraiaFit.UI.currentUser["role"] == "coach")
  {
    document.getElementById("menu-item-users").className = "";
  }
  PraiaFit.Messages.list({
    responseFn : function(obj) {
      var messagesListArea = document.getElementById("praiafit-messages-list");
      messagesListArea.innerHTML = PraiaFit.UI.Messages.convertListToHTML(obj);     
    },
    errorFn : function(jqxhr,status,error)
    {
      var messagesErrorLabel = document.getElementById("praiafit-messages-error");
      messagesErrorLabel.innerHTML = JSON.stringify(jqxhr.responseJSON);
    }
  });
  PraiaFit.Users.list({
    responseFn : function(obj) {
      var messageFormToSelect = document.getElementById("praiafit-message-form-to-select");
      messageFormToSelect.innerHTML = PraiaFit.UI.Users.convertListToOptions(obj);
    }
  });
};

PraiaFit.UI.executeUsersMenuItemAction = function()
{
  PraiaFit.Core.display("praiafit-messages",false);
  PraiaFit.Core.display("praiafit-users",true);
  PraiaFit.Core.display("praiafit-profile",false);
  PraiaFit.Core.display("praiafit-about",false); 
  document.getElementById("menu-item-messages").className = "";
  document.getElementById("menu-item-users").className = "active";
  document.getElementById("menu-item-profile").className = "";
  document.getElementById("menu-item-about").className = "";
  PraiaFit.Users.list({
    responseFn : function(obj) {
      var usersListArea = document.getElementById("praiafit-users-table");
      usersListArea.innerHTML = PraiaFit.UI.Users.convertListToRows(obj);
    }
  });
};

PraiaFit.UI.executeProfileMenuItemAction = function()
{
    PraiaFit.Core.display("praiafit-messages",false);
    PraiaFit.Core.display("praiafit-users",false);
    PraiaFit.Core.display("praiafit-profile",true);
    PraiaFit.Core.display("praiafit-about",false); 
    document.getElementById("menu-item-messages").className = "";
    if (PraiaFit.UI.currentUser["role"] == "coach")
    {
      document.getElementById("menu-item-users").className = "";
    }
    document.getElementById("menu-item-profile").className = "active";
    document.getElementById("menu-item-about").className = "";
};

PraiaFit.UI.executeAboutMenuItemAction = function()
{
    PraiaFit.Core.display("praiafit-messages",false);
    PraiaFit.Core.display("praiafit-users",false);
    PraiaFit.Core.display("praiafit-profile",false);
    PraiaFit.Core.display("praiafit-about",true); 
    document.getElementById("menu-item-messages").className = "";
    if (PraiaFit.UI.currentUser["role"] == "coach")
    {
      document.getElementById("menu-item-users").className = "";
    }
    document.getElementById("menu-item-profile").className = "";
    document.getElementById("menu-item-about").className = "active";
};

PraiaFit.UI.loginAction = function(emailInputId,passwordInputId,errorAreaId)
{
    var email    = document.getElementById(emailInputId).value;
    var password = document.getElementById(passwordInputId).value;
    document.getElementById(emailInputId).value = "";
    document.getElementById(passwordInputId).value = "";

    PraiaFit.Auth.signIn({
      email: email,
      password: password,
      responseFn : function(obj){

        if (obj.errors != undefined)
        {
          var label = document.getElementById(errorAreaId);
          label.innerHTML = "";
          for (index in obj.errors)
          {
            label.innerHTML += obj.errors[index] + " ";
          }
        }
        else
        {
          console.log("login successful");
          window.location.assign("/");      
        }
      }
    });
};

PraiaFit.UI.logoutAction = function()
{
   PraiaFit.Auth.signOut({
      responseFn : function(obj,status) {
        console.log("PraiaFit.Auth.signOut  obj="+JSON.stringify(obj)+" status="+status);
        if (status=="nocontent")
        {
          console.log("logout successful");
          window.location.assign("/");        
        }
      }
    });    
};

PraiaFit.UI.sendMessageAction = function(messageToInputElementId,messageContentTextAreaElementId)
{
  var to_id   = null;
  if (document.getElementById(messageToInputElementId) != undefined)
  {
    to_id = PraiaFit.UI.Core.getSelectedOption(messageToInputElementId);
  }
  var content = document.getElementById(messageContentTextAreaElementId).value;

  PraiaFit.Messages.send({
    content: content,
    to_id: to_id,
    responseFn : function(data){
      window.location.assign("/");
    },
    errorFn : function(jqxhr,status,error)
    {
      var messagesErrorLabel = document.getElementById("praiafit-messages-error");
      messagesErrorLabel.innerHTML = JSON.stringify(jqxhr.responseJSON);
    }
  });
};

// params : Object
// {
//   nameInputElementId : String,
//   emailInputElementId : String,
//   roleSelectElementId : String,
//   passwordInputElementId : String
// }
PraiaFit.UI.createUserAction = function(params)
{
  if (params.nameInputElementId == undefined ||
      params.emailInputElementId == undefined ||
      params.roleSelectElementId == undefined ||
      params.passwordInputElementId == undefined)
  {
    console.log("ERROR: PraiaFit.UI.createUserAction: Invalid params !! params = " + params);
    alert("ERROR: PraiaFit.UI.createUserAction");
    return;
  }

  var userName     = document.getElementById("praiafit-user-form-name-input").value;
  var userEmail    = document.getElementById("praiafit-user-form-email-input").value;
  var userRole     = PraiaFit.UI.Core.getSelectedOption("praiafit-user-form-role-select");
  var userPassword = document.getElementById("praiafit-user-form-password-input").value;

  PraiaFit.Users.create({
    name  : userName,
    email : userEmail,
    role  : userRole,
    password : userPassword,
    responseFn : function(data,status) { 
      window.location.assign("/");         
    },
    errorFn : function(jqxhr,status,error)
    {
      var errorLabel = document.getElementById("praiafit-user-form-error");
      var errors = jqxhr["responseJSON"];
      errorLabel.innerHTML = errors;
    }
  });
};

PraiaFit.UI.editProfileButtonAction = function(profileEmailElementId,profileButtonElementId)
{
  var profileEmailInput = document.getElementById(profileEmailElementId);
  profileEmailInput.disabled = false;
  var profileButton = document.getElementById(profileButtonElementId);
  profileButton.innerHTML = "Atualizar";
  profileButton.onclick = function() { 
    PraiaFit.UI.updateProfileAction(profileEmailElementId,profileButtonElementId);
  };
};

PraiaFit.UI.updateProfileAction = function(profileEmailElementId,profileButtonElementId)
{
  var profileEmailInput = document.getElementById(profileEmailElementId);
  var email = profileEmailInput.value;
  var profileButton = document.getElementById(profileButtonElementId);
  var profileErrorLabel = document.getElementById("profile-form-error");
  PraiaFit.Profile.update({
    email : email,
    responseFn : function() {
      profileEmailInput.disabled = true;
      profileButton.innerHTML = "Editar";
      profileButton.onclick = function() {
        PraiaFit.UI.editProfileButtonAction(profileEmailElementId,profileButtonElementId);
      };
      profileErrorLabel.innerHTML = "";
    },
    errorFn : function(jqxhr) {
      var profileErrorLabel = document.getElementById("profile-form-error");
      profileErrorLabel.innerHTML = JSON.stringify(jqxhr.responseJSON);
    }
  });
};


