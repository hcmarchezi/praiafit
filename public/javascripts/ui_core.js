PraiaFit.UI = { };

PraiaFit.UI.Core = { };

PraiaFit.UI.Core.getSelectedOption = function(selectId) {
  var selectElement = document.getElementById(selectId);
  if (selectElement == undefined)
  {
    console.log("PraiaFit.UI.Core.getSelectedOption: ERROR: Element with id="+selectId+" was not found");
  }
  var selectedValue = null;
  if (selectElement.selectedIndex > -1)
  {
    selectedValue = selectElement.options[selectElement.selectedIndex].value;
  }
  return selectedValue;
};

PraiaFit.Core.setSelectedOption = function(selectId,value) {
  var selectElement = document.getElementById(selectId);
  if (selectElement == undefined)
  {
    console.log("PraiaFit.UI.Core.setSelectedOption: ERROR: Element with id="+selectId+" was not found");
  }
  for (index = 0; index < selectElement.options; index++)
  {
    if (selectElement.options[index] == value)
    {
      selectElement.selectedIndex = index;
      break;
    }
  }
};

PraiaFit.UI.Core.insertUsersMenuItem = function(menuId) {
  var menu = document.getElementById(menuId);
  menu.innerHTML += "<li id=\"menu-item-users\"><a id=\"menu-item-users-link\" href=\"javascript:void(0)\">Usuários</a></li>";
};

PraiaFit.UI.Messages = { };

PraiaFit.UI.Messages.convertListToHTML = function(messages) {
  var html = "";
  for(index=0; index < messages.length; index++)
  {
    var messageItem = messages[index];
    if (messageItem["position"] == "left")
    {
      html += "<div class=\"coach-message\">";
    }
    else
    {
      html += "<div class=\"customer-message\">";
    }
    html += "<p>";
    html += "<div>De <b>" + messageItem["from"] + "</b></div>" 
    html += "<div>Para <b>" + messageItem["to"] + "</b></div>";
    html += "<div>Em <b>" + messageItem["date"] + "</b></div>";
    html += "</p>";
    html += "<p>" + messageItem["content"] + "</p>";
    html += "<ul>";    
    for (indexAtt=0; indexAtt < messageItem["attachments"].length; indexAtt++)
    {
      fileUrl  = messageItem["attachments"][indexAtt];
      html += "<li><a href=\""+fileUrl+"\"><b>"+fileUrl+"</b></a></li>";
    }
    html += "</ul>";
    html += "</div>";
  }
  return html;
};





PraiaFit.UI.Users = { };

PraiaFit.UI.Users.convertListToRows = function(users) {
  var html = "";
  html += "<tr> <th>Nome</th> <th>Email</th> <th>Tipo</th> <th>Senha</th> <th></th> <th></th> </tr>";
  for(index=0; index < users.length; index++)
  {
    var userItem = users[index];
    html += "<tr id=\"praiafit-users-row-"+userItem.id+"\">";
    html += "<input type=\"hidden\" value=\""+userItem.id+"\"></input>";
    html += "<td><input  id=\"praiafit-users-input-name-"+userItem.id+"\" type=\"text\" value=\""+userItem.name+"\" disabled=\"true\"></input></td>";
    html += "<td><input id=\"praiafit-users-input-email-"+userItem.id+"\" type=\"email\" value=\""+userItem.email+"\" disabled=\"true\"></input></td>";
    if (userItem.role == "aluno")
    {
      html += "<td><select  id=\"praiafit-users-select-role-"+userItem.id+"\" disabled=\"true\"><option selected=\"true\">aluno</option><option>treinador</option></select></td>";
    }
    else
    {
      html += "<td><select  id=\"praiafit-users-select-role-"+userItem.id+"\" disabled=\"true\"><option>aluno</option><option selected=\"true\">treinador</option></select></td>";
    }
    html += "<td><input  id=\"praiafit-users-input-password-"+userItem.id+"\" class=\"input-small\" type=\"password\" value=\"XXXXXXXXXX\" disabled=\"true\"></input></td>";
    html += "<td id=\"praiafit-users-buttons-"+userItem.id+"\">";
    html += "<button id=\"praiafit-users-edit-button-"+userItem.id+"\" class=\"btn btn-primary\" onclick=\" PraiaFit.UI.Users.editUserButtonClickAction("+userItem.id+"); \">Editar</button>";
    html += "</td>";
    html += "<td><button class=\"btn btn-danger\"  onclick=\" PraiaFit.UI.Users.removeUserButtonClickAction("+userItem.id+"); \">Remover</button></td>";
    html += "</tr>"

  }
  return html;
}

PraiaFit.UI.Users.convertListToOptions = function(users) {
  var html = "";
  for(index=0; index < users.length; index++)
  {
    var userItem = users[index];
    html += "<option value=\""+userItem.id+"\" >"+userItem.name+"</option>";
  }
  return html;
}

PraiaFit.UI.Users.editUserButtonClickAction = function(userId)
{
  var inputName  = document.getElementById("praiafit-users-input-name-"+userId);
  var inputEmail = document.getElementById("praiafit-users-input-email-"+userId);
  var selectRole = document.getElementById("praiafit-users-select-role-"+userId);
  var inputPassword = document.getElementById("praiafit-users-input-password-"+userId);
  var usersButtonsDiv = document.getElementById("praiafit-users-buttons-"+userId);

  if (inputName == undefined || inputEmail == undefined || selectRole == undefined || inputPassword == undefined || usersButtonsDiv == undefined )
  {
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: some elements were not found");
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputName = "+inputName);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputEmail = "+inputEmail);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: selectRole = "+selectRole);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputPassword = "+inputPassword);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: usersButtonsDiv = "+usersButtonsDiv);
  }  
  inputName.disabled = false;
  inputEmail.disabled = false;
  selectRole.disabled = false;
  inputPassword.disabled = false;
  usersButtonsDiv.innerHTML = "";
  usersButtonsDiv.innerHTML += "<button id=\"praiafit-users-update-button-"+userId+"\" class=\"btn btn-primary\" onclick=\" PraiaFit.UI.Users.updateUserButtonClickAction("+userId+"); \">Atualizar</button>";
  usersButtonsDiv.innerHTML += "<button id=\"praiafit-users-cancel-button-"+userId+"\" class=\"btn btn-warning\" onclick=\" PraiaFit.UI.Users.cancelUserButtonClickAction("+userId+"); \">Cancelar</button>";
};

PraiaFit.UI.Users.updateUserButtonClickAction = function(userId)
{
  var inputName  = document.getElementById("praiafit-users-input-name-"+userId);
  var inputEmail = document.getElementById("praiafit-users-input-email-"+userId);
  var selectRole = document.getElementById("praiafit-users-select-role-"+userId);
  var inputPassword = document.getElementById("praiafit-users-input-password-"+userId);
  var usersButtonsDiv = document.getElementById("praiafit-users-buttons-"+userId);

  if (inputName == undefined || inputEmail == undefined || selectRole == undefined || inputPassword == undefined || usersButtonsDiv == undefined )
  {
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: some elements were not found");
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputName = "+inputName);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputEmail = "+inputEmail);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: selectRole = "+selectRole);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputPassword = "+inputPassword);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: usersButtonsDiv = "+usersButtonsDiv);
  }  
  var name = inputName.value;
  var email = inputEmail.value;
  var role = PraiaFit.UI.Core.getSelectedOption("praiafit-users-select-role-"+userId);
  var password = inputPassword.value;

  var params = {
        id: userId,
        name: name,
        email: email,
        role: role
      };      
  if (password != "XXXXXXXXXX")
  {  
    params.password = password;
  }

  params.responseFn = function(data)
  {
    console.log("User update works! data = "+ JSON.stringify(data));
    inputName.disabled = true;
    inputEmail.disabled = true;
    selectRole.disabled = true;
    inputPassword.disabled = true;
    usersButtonsDiv.innerHTML = "";
    usersButtonsDiv.innerHTML += "<button id=\"praiafit-users-edit-button-"+userId+"\" class=\"btn btn-primary\" onclick=\" PraiaFit.UI.Users.editUserButtonClickAction("+userId+"); \">Editar</button>";
  };

  params.errorFn = function(jqxhr,status,error)
  {
    alert("ERRO: " + JSON.stringify(jqxhr.responseJSON) );
  };

  PraiaFit.Users.update(params);
};

PraiaFit.UI.Users.cancelUserButtonClickAction = function(userId)
{
  var inputName  = document.getElementById("praiafit-users-input-name-"+userId);
  var inputEmail = document.getElementById("praiafit-users-input-email-"+userId);
  var selectRole = document.getElementById("praiafit-users-select-role-"+userId);
  var inputPassword = document.getElementById("praiafit-users-input-password-"+userId);
  var usersButtonsDiv = document.getElementById("praiafit-users-buttons-"+userId);

  if (inputName == undefined || inputEmail == undefined || selectRole == undefined || inputPassword == undefined || usersButtonsDiv == undefined )
  {
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: some elements were not found");
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputName = "+inputName);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputEmail = "+inputEmail);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: selectRole = "+selectRole);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: inputPassword = "+inputPassword);
    console.log("ERROR: PraiaFit.UI.Users.editUserButtonClickAction: usersButtonsDiv = "+usersButtonsDiv);
  }  

  PraiaFit.Users.get({
    id: userId,
    responseFn : function(data) {
      if (data.role == "coach") 
      { 
        data.role = "treinador"; 
      }
      else if (data.role == "customer")
      { 
        data.role = "aluno"; 
      }

      inputName.value = data.name,
      inputEmail.value = data.email,
      PraiaFit.Core.setSelectedOption("praiafit-users-select-role-"+userId,data.role);
      inputPassword.value = "XXXXXXXXXX";
    }
  });  

  inputName.disabled = true;
  inputEmail.disabled = true;
  selectRole.disabled = true;
  inputPassword.disabled = true;
  usersButtonsDiv.innerHTML = "";
  usersButtonsDiv.innerHTML += "<button id=\"praiafit-users-edit-button-"+userId+"\" class=\"btn btn-primary\" onclick=\" PraiaFit.UI.Users.editUserButtonClickAction("+userId+"); \">Editar</button>";
};

PraiaFit.UI.Users.removeUserButtonClickAction = function(userId)
{  
  PraiaFit.Users.delete({
    id : userId,
    responseFn : function(data,status) { 
      if (status=="nocontent")
      {
        PraiaFit.UI.Core.removeRow("praiafit-users-table","praiafit-users-row-"+userId);         
      }
      else
      {
        console.log("ERROR: PraiaFit.UI.Users.removeUserButtonClickAction("+userId+"); data="+data+" status="+status);
      }
    }
  });
};

PraiaFit.UI.Core.removeRow = function(usersTableId,usersTableRowId)
{
  var tableElement = document.getElementById(usersTableId);
  for (index=0; index < tableElement.rows.length; index++)
  {
    var rowElement = tableElement.rows[index];
    if (rowElement.id == usersTableRowId)
    {
      tableElement.deleteRow(index);
      break;
    }
  }
};

PraiaFit.UI.Core.setLoginWallpaper = function()
{
  document.getElementById("praiafit-body").className = "praiafit-login-body";
};

PraiaFit.UI.Core.setAuthenticatedWallpaper = function()
{
  document.getElementById("praiafit-body").className = "praiafit-authenticated-body";
};





