PraiaFit.Users = { };

///////////////////////////////////////////
// List user's ordered by name
// params : Object
//  {
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Users.list = function(params)
{
  PraiaFit.Core.ajaxJSONGet({
    url: "/admin/users",
    responseFn : params.responseFn
  });
};

////////////////////////////////////////////
// Remove user by id
// params : Object
//  {
//    id : user's id to be deleted
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Users.delete = function(params)
{
  if (params.id == undefined || params.responseFn == undefined)
  {
    console.log("ERROR: PraiaFit.User.delete: params.userid="+params.id);
    console.log("ERROR: PraiaFit.User.delete: params.responseFn="+params.responseFn);
    return;
  }

  PraiaFit.Core.ajaxJSONDelete({
    url: "/admin/users/"+params.id,
    responseFn : params.responseFn
  });

};

////////////////////////////////////////////
// Update user data
// params : Object
//  {
//    id : user's id to be updated
//    name : String,
//    email :String,
//    role : String,
//    password : String,
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Users.update = function(params)
{
  if (params.role == "aluno")
  {
    params.role == "customer";
  }
  else if (params.role == "treinador")
  {
    params.role == "coach";
  }
  post_data = "";
  post_data += "user[name]="+params.name;
  post_data += "&user[email]="+params.email;
  post_data += "&user[role]="+params.role;
  if (params.password != undefined)
  {
    post_data += "&user[password]="+params.password;
    post_data += "&user[password_confirmation]="+params.password;
  }

  PraiaFit.Core.ajaxJSONPut({
    url : "/admin/users/"+params.id,
    post_data : post_data,
    responseFn : params.responseFn,
    errorFn : params.errorFn
  });

};


////////////////////////////////////////////
// Get data from one user by id
// params : Object
//  {
//    id : user's id to be selected
//    responseFn : function(data) { ...... }
//  }
PraiaFit.Users.get = function(params)
{  
  PraiaFit.Core.ajaxJSONGet({
    url : "/admin/users/"+params.id,
    responseFn : params.responseFn
  });
};

////////////////////////////////////////////
// Create a new user 
// params : Object
//  {
//    name  : String,
//    email : String,
//    role  : String,
//    password : String,
//    responseFn : function(data) { ...... },
//    errorFn : function(jqxhr,status,error) { ......... }
//  }
PraiaFit.Users.create = function(params)
{
  if (params.role == "aluno")
  {
    params.role == "customer";
  }
  else if (params.role == "treinador")
  {
    params.role == "coach";
  }
  post_data = "";
  post_data += "user[name]="+params.name;
  post_data += "&user[email]="+params.email;
  post_data += "&user[role]="+params.role;
  post_data += "&user[password]="+params.password;
  post_data += "&user[password_confirmation]="+params.password;

  PraiaFit.Core.ajaxJSONPost({
    url : "/admin/users/",
    post_data : post_data,
    responseFn : params.responseFn,
    errorFn : params.errorFn,
  });
};


