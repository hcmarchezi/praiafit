Fabricator(:attachment) do
  message  { Fabricator(:coach_message) }
  filename { "/path/to/filename.ext" } 
end
