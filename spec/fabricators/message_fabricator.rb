
Fabricator(:customer_message, from: :message) do  
  date     { Time.now }
  content  "some random message content"
  from     { Fabricate(:customer_user) }
end

Fabricator(:coach_message, from: :coach_message) do
  date     { Time.now }
  content  "some random message content"
  from     { Fabricate(:coach_user) }
  to       { Fabricate(:customer_user) }
end




