Fabricator(:customer_user, from: :user) do
  name  { sequence(:name) { |i| "Customer#{i}" } }
  email { sequence(:email) { |i| "customer#{i}@test.com" } }
  role  :customer # { Role.customer }
  password "12345678"
  password_confirmation "12345678"   # "#{attrs[:password]}"
end

  # This will use the User class (Admin would have been guessed)
Fabricator(:coach_user, from: :user) do
  name  { sequence(:name) { |i| "Coach#{i}" } }
  email { sequence(:email) { |i| "coach#{i}@test.com" } }
  role  :coach # { Role.coach }
  password "12345678"
  password_confirmation "12345678"
end


