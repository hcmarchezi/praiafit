require 'spec_helper'

describe "Attachment" do

  context "creation" do
    before do
      @customer = Fabricate(:customer_user)
      File.open("tempfile.tmp","w") { |file| 
        file.write "0123456789" 
      }
      @file = ActionDispatch::Http::UploadedFile.new(
        :filename => "test.txt",
        :type => "application/pdf",
        :head => "",
        :tempfile => File.new("tempfile.tmp","r") )
      @message = Fabricate(:coach_message)
      @dir_path  = "#{Rails.root}#{APP_CONFIG['file_path']}#{@message.id}/"
      @file_path = @dir_path + "test.txt"
    end

    it "should create an object with a file path and save a file in correct location " do 
      attachment = Attachment.new
      attachment.message = @message
      attachment.filename = @file.original_filename
      attachment.save_uploaded_file(@file)
      attachment.save
      attachment.errors.full_messages.should == []      
      File.exists?(@file_path).should == true
      content = ""
      File.open(@file_path) { |file|
        content = file.read
      }     
      content.should == "0123456789"
    end

    it "should require a file path" do
      attachment = Attachment.create()
      attachment.errors.full_messages.should == [ 
        I18n.t("models.attachment.blank_message"), 
        I18n.t("models.attachment.blank_filename") 
      ]
    end

    after do
      File.delete("tempfile.tmp") if File.exists?("tempfile.tmp")
      FileUtils.rm_rf(@dir_path)  if File.directory?(@dir_path)
    end

  end


end
