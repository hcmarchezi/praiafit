require 'spec_helper'

describe CoachMessage do
  before do
#    @customer_role = Role.create( :name => "Customer" )
#    @coach_role = Role.create( :name => "Coach" )
    @customer_user = Fabricate ( :customer_user )
    @coach_user = Fabricate( :coach_user )
    @other_customer_user = Fabricate( :customer_user )
    @other_coach_user = Fabricate( :coach_user )
  end

  it "should create an object with properties date, content, from user, to user when coach sends it to customer" do
    message = CoachMessage.create(
        :date => "2013-05-16 18:10:00",
        :content => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac dapibus magna. Sed in dui eget massa tristique scelerisque vitae ut urna.",
        :from => @coach_user,
        :to => @customer_user )
    message.errors.full_messages.should == [ ]
  end

  it "forbids itself from being sent from coach to coach" do
    message = CoachMessage.create(
        :date => "2013-06-01 12:30:45",
        :content => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac dapibus magna. Sed in dui eget massa tristique scelerisque vitae ut urna.",
        :from => @coach_user,
        :to => @other_coach_user )
    message.errors.full_messages.should == [ I18n.t("models.message.coach_to_coach") ]
  end

end
