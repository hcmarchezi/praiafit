require 'spec_helper'

describe Message do
  before do
#    @customer_role = Role.create( :name => "Customer" )
#    @coach_role = Role.create( :name => "Coach" )
    @customer_user = Fabricate ( :customer_user )
    @coach_user = Fabricate( :coach_user )
    @other_customer_user = Fabricate( :customer_user )
    @other_coach_user = Fabricate( :coach_user )
  end

  it "should create an object with properties title, content when customer sends it to coach" do
    message = Message.create(
        :date => "2013-05-16 17:15:00",
        :content => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac dapibus magna. Sed in dui eget massa tristique scelerisque vitae ut urna.",
        :from => @customer_user)
    message.errors.full_messages.should == [ ]
  end

  it "requires date, content, from user" do
    message = Message.create()
    message.errors.full_messages.should == [ 
      I18n.t("models.message.blank_date"),
      I18n.t("models.message.blank_content"),
      I18n.t("models.message.blank_from")
    ]
  end

end
