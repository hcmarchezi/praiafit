require 'spec_helper'

describe "User" do

#  before(:all) do
#    customer_role = Role.create( :name => "Customer" )
#    coach_role = Role.create( :name => "Coach" )  
#  end

  context "creation" do

#    before do    
#      @role = Role.first
#    end

    it "should create a new object with name, email, password and role" do
            user = User.create( 
                :name => "John Doe", 
                :email => "john.doe@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678",
                :role => :customer ) 
            user.name.should == "John Doe"
            user.email.should == "john.doe@test.com"
            user.password.should == "12345678"
            #user.role.name.should == @role.name
            #user.role.id.should == @role.id
            user.role.should == :customer
            user.errors.size.should == 0
    end

    it "should require name" do
            user = User.create( 
                :email => "john.doe@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678", 
                :role => :customer ) 
            user.errors.full_messages.should == [ I18n.t("models.user.blank_name"), I18n.t("models.user.name_size") ]
    end

    it "should require email" do
            user = User.create( 
                :name => "John Doe", 
                :password => "12345678", 
                :password_confirmation => "12345678", 
                :role => :customer ) 
            user.errors.full_messages.should == [ I18n.t("models.user.blank_email") ]
    end

    it "should require password" do
            user = User.create( 
                :name => "John Doe", 
                :email => "john.doe@test.com", 
                :role => :customer  ) 
            user.errors.full_messages.should == [ I18n.t("models.user.blank_encrypted_password") ]
    end

    it "should require role" do
            user = User.create( 
                :name => "John Doe", 
                :email => "john.doe@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678")
            user.errors.full_messages.should == [ I18n.t("models.user.blank_role") ] 
    end

    it "should not accept names smaller than 5 characters" do
            user = User.create(
                :name => "1234", 
                :email => "john.doe@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678",
                :role => :customer  ) 
            user.errors.full_messages.should == [ I18n.t("models.user.name_size") ]
    end

    it "should not accept names bigger than 120 characters" do
            user = User.create(
                :name => "Josef Magnan Mangoldoff Melatonin Jackabollic Mergadonovich Yertolomerapiumakatorapeutalonick Josef Magnan Mangoldoff Melatonin Jackabollic Mergadonovich Yertolomerapiumakatorapeutalonick", 
                :email => "john.doe@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678",
                :role => :customer ) 
            user.errors.full_messages.should == [ I18n.t("models.user.name_size") ]
    end

    it "should not accept email taken from other user" do
            first_user = Fabricate(:customer_user, :email => "12345@test.com")
            second_user = User.create(
                :name => "John Doe", 
                :email => "12345@test.com", 
                :password => "12345678", 
                :password_confirmation => "12345678",
                :role => :customer  ) 
            second_user.errors.full_messages.should == [ I18n.t("models.user.not_unique_email") ]
    end
     
    it "should not accept invalid email format" do
            user = User.create(
                :name => "John Doe", 
                :email => "invalid##email@format", 
                :password => "12345678", 
                :password_confirmation => "12345678",
                :role => :customer  ) 
            user.errors.full_messages.should == [ I18n.t("models.user.email_format") ]
    end
  end


  context "customer and coach queries" do
    before do
      @customer1 = Fabricate(:customer_user)
      @coach1    = Fabricate( :coach_user  )
      @customer2 = Fabricate( :customer_user )
      @coach2    = Fabricate( :coach_user )
      @customer3 = Fabricate( :customer_user )
      @customer4 = Fabricate( :customer_user )
    end  

    it "should list all customers" do
      customers = User.customers
      customers.should == [ @customer1, @customer2, @customer3, @customer4 ]
    end

    it "should list all coaches" do
      coaches = User.coaches
      coaches.should == [ @coach1, @coach2 ]
    end
  end

end


