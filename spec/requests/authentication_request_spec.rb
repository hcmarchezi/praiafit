require 'spec_helper'

describe "User authentication" do

  context "sign in with wrong email" do
    it "should return a wrong email or password error message" do      
      post "/users/sign_in.json", { "email" => "nonexistent@test.com", "password" => @password }   
      expect(JSON.parse(response.body)).to eq({ "errors" => [ I18n.t("devise.failure.invalid") ] })     
    end 
  end

  context "sign in with wrong password" do
    it "should return a wrong email or password error message" do
      post "/users/sign_in.json", { "email" => @email, "password" => "wrongpass" }  
      expect(JSON.parse(response.body)).to eq({ "errors" => [ I18n.t("devise.failure.invalid") ] })      
    end
  end

  context "sign out after sign in" do
    it "should return a response without header" do
      post "/users/sign_in.json", { "user" => { "email" => @email , "password" => @password } }
      delete "/users/sign_out.json"
      response.code.should == "204"
    end
  end

  context "sign out without sign in" do
    it "should return a response without error" do
      delete "/users/sign_out.json"
      response.code.should == "204"
    end
  end

end

describe "Customer authentication" do

  context "sign in" do    

    before do
      @email = "carlos@test.com"
      @name = "Carlos Alberto"
      @password = "12345678"
      Fabricate(
        :customer_user, 
        :email => @email, 
        :name => @name,
        :password => @password, 
        :password_confirmation => @password)
      @user = User.first     
    end 

    it "should return user information" do  
      post "/users/sign_in.json", { "user" => { "email" => @email , "password" => @password } }
      expect(JSON.parse(response.body)).to eq(
        { 
          "name" => @name, 
          "email" => @email,
          "features" => [ "messaging", "profile" ]
        })
    end   
  end

end


describe "Coach authentication" do

  before do
    @email = "jptrainner@email.com"
    @name = "JPTrainner"
    @password = "12345678"
    Fabricate(
      :coach_user, 
      :email => @email, 
      :name => @name,
      :password => @password, 
      :password_confirmation => @password)
    @user = User.first     
  end  

  context "sign in" do    
    it "should return user information" do  
      post "/users/sign_in.json", { "user" => { "email" => @email , "password" => @password } }
      expect(JSON.parse(response.body)).to eq(
        { 
          "name" => @name, 
          "email" => @email,
          "features" => [ "messaging", "profile", "user management" ]
        })
    end   
  end

end


