# encoding: utf-8

require "spec_helper"

describe "File download" do
  describe "without authenticated user" do
    before do
      @msg = Fabricate(:coach_message)
      @attachment1 = Fabricate(:attachment, :message => @msg, :filename => "file1.ext")
      get "/download/#{@msg.id}/attachment.json?filename=file1.ext"
    end
    it "should return unauthorized access error" do
      response.code.should be_eql("401")
    end
  end
  describe "with authenticated user other then the message's origin or destination" do
    before do
      @msg = Fabricate(:coach_message)
      @attachment1 = Fabricate(:attachment, :message => @msg, :filename => "file1.ext")
      @user = Fabricate(:customer_user, :password => "12345678", :password_confirmation => "12345678")      
      post "/users/sign_in.json", { "user" => { "email"=>@user.email, "password"=>"12345678" } }
      get  "/download/#{@msg.id}/attachment.json?filename=file1.ext"
    end
    it "should return unauthorized access error" do
      response.code.should be_eql("401")
    end
  end
  describe "with authenticated user in message's destination" do
    before do
      @user = Fabricate(:customer_user, :password => "12345678", :password_confirmation => "12345678")      
      @msg = Fabricate(:coach_message, :to => @user)
      @attachment1 = Fabricate(:attachment, :message => @msg, :filename => "file1.ext")
      file_path = "#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext"
      create_file(file_path,"#### file contents ####")
      post "/users/sign_in.json", { "user" => { "email"=>@user.email, "password"=>"12345678" } }
      get  "/download/#{@msg.id}/attachment.json?filename=file1.ext"
    end
    it "should download file contents" do
      response.code.should be_eql("200")
      response.body.should be_eql("#### file contents ####")
    end
    after do
      File.delete("#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext")
    end
  end
  describe "with authenticated user in message's origin" do
    before do
      @user = Fabricate(:coach_user)      
      @msg = Fabricate(:coach_message, :from => @user)
      @attachment1 = Fabricate(:attachment, :message => @msg, :filename => "file1.ext")
      file_path = "#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext"
      create_file(file_path,"#### file contents ####")
      post "/users/sign_in.json", { "user" => { "email"=>@user.email, "password"=>"12345678" } }
      get  "/download/#{@msg.id}/attachment.json?filename=file1.ext"
    end
    it "should download file contents" do
      response.code.should be_eql("200")
      response.body.should be_eql("#### file contents ####")
    end
    after do
      File.delete("#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext")
    end
  end
  describe "with authenticated user in message's origin and filename with 2 dots" do
    before do
      @user = Fabricate(:coach_user)      
      @msg = Fabricate(:coach_message, :from => @user)
      @attachment1 = Fabricate(:attachment, :message => @msg, :filename => "file1.ext1.ext2")
      file_path = "#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext1.ext2"
      create_file(file_path,"#### file contents ####")
      post "/users/sign_in.json", { "user" => { "email"=>@user.email, "password"=>"12345678" } }
      get  "/download/#{@msg.id}/attachment.json?filename=file1.ext1.ext2"
    end
    it "should download file contents" do
      response.code.should be_eql("200")
      response.body.should be_eql("#### file contents ####")
    end
    after do
      File.delete("#{Rails.root}#{APP_CONFIG['file_path']}#{@msg.id}/file1.ext1.ext2")
    end  
  end


  def create_file(file_path,contents)

    directory = File.dirname(file_path)

    if File.directory?(directory) == false
      Dir.mkdir(directory)     
    end

    File.open(file_path,"w") { |outfile|
      outfile.write contents
    }

  end
end
