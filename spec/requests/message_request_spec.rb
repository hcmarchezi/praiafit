# encoding: utf-8

require 'spec_helper'

describe "Message" do
  describe "listing" do
    context "with unauthenticated user" do
      before do
        get "/messages", { format: "json" }
      end
      it "should return access denied error" do
        response.code.should == "401"     
      end
    end  
    context "with authenticated customer" do
      before do
        @authenticated_customer = Fabricate( :customer_user, :name => "John Doe", :email => "john.doe@email.com", :password => "12345678", :password_confirmation => "12345678" )
        @authenticated_customer_id = @authenticated_customer.id.to_s
        @jptrainner_coach = Fabricate( :coach_user, :name => "JPTrainner" )
        @jptrainner_coach_id = @jptrainner_coach.id.to_s
        @socioJP_coach = Fabricate( :coach_user, :name => "SocioJP" )
        @socioJP_coach_id = @socioJP_coach.id.to_s
        other_customer = Fabricate( :customer_user, :name => "Ed Harris" )
        @msg1 = Fabricate( :customer_message, :from => @authenticated_customer,                                 :date => Time.new(2013,5,25, 9, 4,23), :content => "My second customer message")
        @msg2 = Fabricate( :coach_message,    :from => @jptrainner_coach,       :to => @authenticated_customer, :date => Time.new(2013,5,20,17,15,13), :content => "Shutup and do as I say")    
        Attachment.create( :message => @msg2, :filename => "file1.pdf" ) 
        @msg3 = Fabricate( :customer_message, :from => @authenticated_customer,                                 :date => Time.new(2013,5,20,15,20,20), :content => "My first customer message")
        @msg4 = Fabricate( :coach_message,    :from => @jptrainner_coach,       :to => @authenticated_customer, :date => Time.new(2013,5,26,11,10, 3), :content => "I just read it\r\nSecond line")
        @msg5 = Fabricate( :coach_message,    :from => @socioJP_coach,          :to => @authenticated_customer, :date => Time.new(2013,5,20,18,20,10), :content => "Follow JP's order")
        Attachment.create( :message => @msg5, :filename => "file2.xls" )
        Attachment.create( :message => @msg5, :filename => "file3 space.doc" )
        @msg6 = Fabricate( :customer_message, :from => @authenticated_customer,                                 :date => Time.new(2013,5,26,10, 0, 5), :content => "My other message")
        @msg7 = Fabricate( :customer_message, :from => other_customer,                                          :date => Time.new(2013,5,31, 9, 0,23), :content => "You should not see this")
        post "/users/sign_in.json", { "user" => { "email" => "john.doe@email.com", "password" => "12345678" } } 
        get "/messages.json"
      end
      it "should return a list of messages" do
        response.code.should == "200"
        json_object = JSON.parse(response.body)
        json_object.should == [
          { "from_id" => @authenticated_customer_id, "from" => "John Doe",   "to" => "treinadores",                "to_id" => "0", "date" => "2013/05/20 15:20:20", "content" => "My first customer message", "position" => "left", "attachments"=>[] },
          { "from_id" => @jptrainner_coach_id,       "from" => "JPTrainner", "to" => @authenticated_customer.name, "to_id" => @authenticated_customer.id.to_s, "date" => "2013/05/20 17:15:13", "content" => "Shutup and do as I say", "position" => "right", "attachments"=>[ "/download/"+@msg2.id.to_s+"/attachment?filename=file1.pdf" ] },
          { "from_id" => @socioJP_coach_id,          "from" => "SocioJP",    "to" => @authenticated_customer.name, "to_id" => @authenticated_customer.id.to_s, "date" => "2013/05/20 18:20:10", "content" => "Follow JP's order", "position" => "right", "attachments"=>[ "/download/"+@msg5.id.to_s+"/attachment?filename=file2.xls" , "/download/"+@msg5.id.to_s+"/attachment?filename=file3 space.doc" ] },
          { "from_id" => @authenticated_customer_id, "from" => "John Doe",   "to" => "treinadores",                "to_id" => "0", "date" => "2013/05/25 09:04:23", "content" => "My second customer message", "position" => "left", "attachments"=>[] },
          { "from_id" => @authenticated_customer_id, "from" => "John Doe",   "to" => "treinadores",                "to_id" => "0", "date" => "2013/05/26 10:00:05", "content" => "My other message", "position" => "left", "attachments"=> [] },
          { "from_id" => @jptrainner_coach_id,       "from" => "JPTrainner", "to" => @authenticated_customer.name, "to_id" => @authenticated_customer.id.to_s, "date" => "2013/05/26 11:10:03", "content" => "I just read it\r\nSecond line", "position" => "right","attachments"=>[] }
        ]
      end
    end  
    context "with authenticated coach" do
      before do
        @authenticated_coach = Fabricate( :coach_user, :name => "JPTrainner", :email => "jptrainner@email.com", :password => "12345678", :password_confirmation => "12345678" )
        @zecarlos_customer = Fabricate( :customer_user, :name => "Jose Carlos Araujo" )
        @melinda_customer = Fabricate( :customer_user, :name => "Melinda Gates" )
        @other_coach = Fabricate( :coach_user, :name => "Socio JP" )
        @authenticated_coach_id = @authenticated_coach.id.to_s
        @zecarlos_customer_id = @zecarlos_customer.id.to_s
        @melinda_customer_id = @melinda_customer.id.to_s
        @other_coach_id = @other_coach.id.to_s
        @msg1 = Fabricate( :customer_message, :from => @zecarlos_customer,                              :date => Time.new(2013,5,20, 8, 3,12), :content => "Good morning coach!")
        @msg2 = Fabricate( :coach_message,    :from => @authenticated_coach, :to => @zecarlos_customer, :date => Time.new(2013,5,20, 9, 5, 3), :content => "Shutup and do as I say")
        att1 = Attachment.create( :message => @msg2, :filename => "anotherfile.xls" )
        @msg3 = Fabricate( :customer_message, :from => @melinda_customer,                               :date => Time.new(2013,5,21,15,20,20), :content => "My knee is in pain")
        @msg4 = Fabricate( :coach_message,    :from => @other_coach,         :to => @melinda_customer,  :date => Time.new(2013,5,21,16,10, 3), :content => "Put your knee in ice")
        @msg5 = Fabricate( :coach_message,    :from => @authenticated_coach, :to => @melinda_customer,  :date => Time.new(2013,5,21,18,20,10), :content => "Don't do exercises")
        att2 = Attachment.create( :message => @msg5, :filename => "file.dat" )
        att3 = Attachment.create( :message => @msg5, :filename => "something.pdf" )
        @msg6 = Fabricate( :customer_message, :from => @melinda_customer,                               :date => Time.new(2013,5,27,10, 0, 5), :content => "I don't feel my leg")
        @msg7 = Fabricate( :customer_message, :from => @zecarlos_customer,                              :date => Time.new(2013,5,27,12, 0,13), :content => "I have a headache\r\nSecond line")
        post "/users/sign_in.json", { "user" => { "email" => "jptrainner@email.com", "password" => "12345678" } } 
        get "/messages.json"
      end
      it "should return a list of messages" do
        response.code.should == "200"
        json_object = JSON.parse(response.body)
        json_object.should == [
          { "from_id" => @zecarlos_customer_id,   "from" => "Jose Carlos Araujo", "to" => "treinadores",           "to_id" => "0", "date" => "2013/05/20 08:03:12", "content" => "Good morning coach!", "position" => "left", "attachments"=>[] },
          { "from_id" => @authenticated_coach_id, "from" => "JPTrainner",         "to" => @zecarlos_customer.name, "to_id" => @zecarlos_customer.id.to_s, "date" => "2013/05/20 09:05:03", "content" => "Shutup and do as I say", "position" => "right", "attachments"=>[ "/download/#{@msg2.id}/attachment?filename=anotherfile.xls" ] },        
          { "from_id" => @melinda_customer_id,    "from" => "Melinda Gates",      "to" => "treinadores",           "to_id" => "0", "date" => "2013/05/21 15:20:20", "content" => "My knee is in pain", "position" => "left", "attachments"=>[] },
          { "from_id" => @other_coach_id,         "from" => "Socio JP",           "to" => @melinda_customer.name,  "to_id" => @melinda_customer.id.to_s, "date" => "2013/05/21 16:10:03", "content" => "Put your knee in ice", "position" => "right", "attachments"=>[] },
          { "from_id" => @authenticated_coach_id, "from" => "JPTrainner",         "to" => @melinda_customer.name,  "to_id" => @melinda_customer.id.to_s, "date" => "2013/05/21 18:20:10", "content" => "Don't do exercises", "position" => "right", "attachments"=>[ "/download/#{@msg5.id}/attachment?filename=file.dat", "/download/#{@msg5.id}/attachment?filename=something.pdf" ] },
          { "from_id" => @melinda_customer_id,    "from" => "Melinda Gates",      "to" => "treinadores",           "to_id" => "0", "date" => "2013/05/27 10:00:05", "content" => "I don't feel my leg", "position" => "left", "attachments"=>[] },
          { "from_id" => @zecarlos_customer_id,   "from" => "Jose Carlos Araujo", "to" => "treinadores",           "to_id" => "0", "date" => "2013/05/27 12:00:13", "content" => "I have a headache\r\nSecond line", "position" => "left", "attachments"=>[] }
        ]
      end
    end  
  end
  describe "creation" do
    context "with unauthenticated user" do
      before do        
      end
      it "should return access denied error" do
        post "/messages", { format: "json" }
        response.code.should == "401"
      end
    end
    context "with authenticated customer" do
      before do
        @customer = Fabricate(:customer_user, :email => "customer@email.com", :password => "12345678", :password_confirmation => "12345678")
        post "/users/sign_in.json", { "user" => { "email" => "customer@email.com", "password" => "12345678" } }
        post "/messages", { "message" => { "content"=>"My customer message" }, format: "json" }
      end
      it "should post a message" do
        response.code.should == "201"
        JSON.parse(response.body).should == {
          "from_id" => @customer.id.to_s,
          "from" => @customer.name,
          "date" => Message.first.date.strftime("%Y/%m/%d %H:%M:%S"),
          "content" => "My customer message",
          "attachments" => [ ]
        }
      end
    end
    context "with authenticated coach" do
      before do
        @customer = Fabricate(:customer_user)
        @coach = Fabricate(:coach_user)
        post "/users/sign_in.json", { "user" => { "email" => @coach.email, "password" => @coach.password } }
        post "/messages", { "message" => { "content"=>"My customer message", "to_id" => @customer.id }, format: "json" }
      end
      it "should post a message to a customer" do
        response.code.should == "201"
        JSON.parse(response.body).should == {
          "from_id" => @coach.id.to_s,
          "from" => @coach.name,
          "to" => @customer.name,
          "date" => Message.first.date.strftime("%Y/%m/%d %H:%M:%S"),
          "content" => "My customer message",
          "attachments" => [ ]
        }
      end 
    end
  end

  describe "creation including attachments" do
    context "with unauthenticated user" do
      before do        
      end
      it "should return access denied error" do
        post "/messages", { format: "json" }
        response.code.should == "401"
      end
    end
    context "with authenticated coach" do
      before do
        @customer = Fabricate(:customer_user)
        @coach = Fabricate(:coach_user)
        post "/users/sign_in.json", { "user" => { "email" => @coach.email, "password" => @coach.password } }
        post "/messages.json", 
          { 
            "message" => { "content"=>"My customer message", "to_id" => @customer.id },
            "attachment1" => Rack::Test::UploadedFile.new("spec/requests/myfile1.txt","text/plain"),
            "attachment2" => Rack::Test::UploadedFile.new("spec/requests/myfile2.txt","text/plain")
          }
      end
      it "should post a message to a customer" do
        response.code.should == "201"
        JSON.parse(response.body).should == {
          "from_id" => @coach.id.to_s,
          "from" => @coach.name,
          "to" => @customer.name,
          "date" => Message.first.date.strftime("%Y/%m/%d %H:%M:%S"),
          "content" => "My customer message",
          "attachments" => [ "myfile1.txt", "myfile2.txt" ]
        }
      end 
      after do
        message = Message.first
        dir_path = "#{Rails.root}#{APP_CONFIG['file_path']}#{message.id}/"
        FileUtils.rm_rf(dir_path)  if File.directory?(dir_path)
      end
    end
  end


end

