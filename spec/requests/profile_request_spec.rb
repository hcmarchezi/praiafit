# encoding: utf-8

require "spec_helper"

describe "Update user profile without authentication" do
  before do
    put "/users/profile.json", { "name" => "Josefina", "email" => "josefina@email.com" }
  end  
  it "should return an error message" do    
    response.code.should == "401"
  end
end

describe "Update user profile with authenticated customer" do
  before do
    Fabricate(:customer_user, :email => "customer@email.com", :password=> "12345678", :password_confirmation=>"12345678" )
    post "/users/sign_in.json", { "user" => { "email" => "customer@email.com", "password" => "12345678" } }
    put "/users/profile.json", { "user" => { "name" => "Josefina", "email" => "josefina@email.com" } }    
  end
  it "should change personal information" do    
    response.code.should == "200"
    JSON.parse(response.body).should == {
      "name" => "Josefina",
      "email" => "josefina@email.com"
    }
  end
end

describe "Update user profile with authenticated coach" do
  before do
    Fabricate(:coach_user, :email => "coach@email.com", :password=> "12345678", :password_confirmation=>"12345678" )
    post "/users/sign_in.json", { "user" => { "email" => "coach@email.com", "password" => "12345678" } }
    put "/users/profile.json", { "user" => { "name" => "Josefina", "email" => "josefina@email.com" } }
  end
  it "should change personal information" do
    response.code.should == "200"
    JSON.parse(response.body).should == {
      "name" => "Josefina",
      "email" => "josefina@email.com"
    }
  end
end
