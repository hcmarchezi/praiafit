# encoding: utf-8

require "spec_helper"

describe "Without authenticated user" do
  describe "customer user registration" do
    it "should return unauthorized access error" do
      post "/admin/users.json", { 
        "user" => {
          "name" => "James Clark", 
          "email" => "james.clark@test.com", 
          "role" => "aluno",
          "password" => "12345678",
          "password_confirmation" => "12345678"
        }
      }
      
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "coach user registration" do
    it "should return unauthorized access error" do
      post "/admin/users.json", { 
        "name" => "James Clark", 
        "email" => "james.clark@test.com", 
        "role" => "treinador",
        "password" => "12345678",
        "password_confirmation" => "12345678"
      }

      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "user edition" do
    before do
      @user = Fabricate(:customer_user)
    end
    it "should return unauthorized access error" do
      put "/admin/users/1.json", { 
        "id" => @user.id,
        "name" => "John Doe", 
        "email" => "john.doe@test.com", 
        "role" => "aluno",
        "password" => "newpassword"
      }

      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end  
  describe "user deletion" do
    before do
      @user = Fabricate(:customer_user)
    end
    it "should return unauthorized access error" do
      delete "/admin/users/1.json", { "id" => @user_id }
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end
  describe "users listing" do
    it "should return unauthorized access error" do
      get "/admin/users.json"
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "user show" do
    it "should return unauthorized access error" do
      get "/admin/users/1.json"
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end
end


describe "With customer user" do
  before(:all) do
    @customer_user = User.create(:name => "Customer", :email=>"customer@test.com", :role => :customer, :password => "12345678", :password_confirmation => "12345678")
    post "users/sign_in.json", { "user" => { "email" => "customer@test.com", "password" => "12345678" } }  
  end
  describe "customer user registration" do
    it "should return unauthorized access error" do
      post "/admin/users.json", { 
        "user" => {
          "name" => "James Clark", 
          "email" => "james.clark@test.com", 
          "role" => "aluno",
          "password" => "12345678",
          "password_confirmation" => "12345678"
        }
      }
      
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "coach user registration" do
    it "should return unauthorized access error" do
      post "/admin/users.json", { 
        "name" => "James Clark", 
        "email" => "james.clark@test.com", 
        "role" => "treinador",
        "password" => "12345678",
        "password_confirmation" => "12345678"
      }

      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "user edition" do
    before do
      @user = Fabricate(:customer_user)
    end
    it "should return unauthorized access error" do
      put "/admin/users/1.json", { 
        "id" => @user.id,
        "name" => "John Doe", 
        "email" => "john.doe@test.com", 
        "role" => "aluno",
        "password" => "newpassword"
      }

      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end  
  describe "user deletion" do
    before do
      @user = Fabricate(:customer_user)
    end
    it "should return unauthorized access error" do
      delete "/admin/users/1.json", { "id" => @user_id }
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end
  describe "users listing" do
    it "should return unauthorized access error" do
      get "/admin/users.json"
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end    
  end
  describe "user show" do
    it "should return unauthorized access error" do
      get "/admin/users/1.json"
      response.code.should == "401"
      JSON.parse(response.body).should == { "errors" => [ I18n.t("controllers.application_controller.access_denied") ] }
    end
  end
  after(:all) do
    User.all.destroy
  end
end




describe "With coach user" do
  before(:all) do
    @coach_user = User.create(:name => "Coach", :email=>"coach@test.com", :role => :coach, :password => "12345678", :password_confirmation => "12345678")
    post "/users/sign_in.json", { "user" => { "email" => "coach@test.com", "password" => "12345678" } }  
  end
  describe "customer user registration" do
    it "should create it" do
      post "/admin/users.json", { 
        "user" => {
          "name" => "James Clark", 
          "email" => "james.clark@test.com", 
          "role" => I18n.t("models.role.customer"),
          "password" => "12345678"
        }
      }      
      JSON.parse(response.body).should == {
        "name" => "James Clark",
        "email" => "james.clark@test.com",
        "role" => I18n.t("models.role.customer")
      }
      response.code.should == "201"
    end    
  end
  describe "coach user registration" do
    it "should create it" do
      post "/admin/users.json", { 
        "user" => {
          "name" => "James Clark", 
          "email" => "james.clark@test.com", 
          "role" => I18n.t("models.role.coach"),
          "password" => "12345678"
        }
      }
      JSON.parse(response.body).should == {
        "name" => "James Clark",
        "email" => "james.clark@test.com",
        "role" => I18n.t("models.role.coach")
      }
      response.code.should == "201"
    end    
  end
  describe "user edition" do
    before do
      @user = Fabricate(:customer_user)
      @user_id = @user.id.to_s
    end
    it "should update existing data" do
      put "/admin/users/#{@user_id}.json", 
      { 
     "user" => {
        "name" => "John Doe", 
        "email" => "john.doe@test.com", 
        "role" => I18n.t("models.role.coach"),
        "password" => "newpassword",
        "password_confirmation" => "newpassword"
       }
      }
      response.code.should == "200"
      JSON.parse(response.body).should == { 
        "name" => "John Doe",
        "email" => "john.doe@test.com",
        "role" => I18n.t("models.role.coach")
      }
    end
  end  
  describe "user deletion" do
    before do
      coach_user = Fabricate(:coach_user)
      @coach_user_id = coach_user.id.to_s
    end
    it "should remove it" do
      delete "/admin/users/"+@coach_user_id+".json"
      response.code.should == "204"
    end
  end
  describe "deletion of user with messages associated" do
    before do
      customer_user = Fabricate(:customer_user)
      @customer_user_id = customer_user.id
      Fabricate(:customer_message, :from => customer_user, :date => Time.new(2013,6,19,9,10,5))
      Fabricate(:coach_message, :from => @coach_user, :to => customer_user, :date => Time.new(2013,6,19,13,10,5))
      Fabricate(:customer_message, :from => customer_user, :date => Time.new(2013,6,20,10,10,10))
      Fabricate(:coach_message, :from => @coach_user, :to => customer_user, :date => Time.new(2013,6,20,15,0,5))
      Fabricate(:customer_message, :from => customer_user, :date => Time.new(2013,6,30,15,12,25))
      Fabricate(:coach_message, :from => @coach_user, :to => customer_user, :date => Time.new(2013,6,30,23,20,5))
    end
    it "should remove it as well as the messages" do
      delete "/admin/users/"+@customer_user_id.to_s+".json"
      response.code.should == "204"
      User.get(@customer_user_id).should be_nil
      Message.all(:from_id => @customer_user_id).to_a.should be_empty
      CoachMessage.all(:to_id => @customer_user_id).to_a.should be_empty
    end
  end
  describe "users listing" do
    before do
      @bonovox_user    = Fabricate(:coach_user, :name => "Bonovox Magna Opus Dei", :email => "pericles.silva@yahoo.com.br")
      @josecarlos_user = Fabricate(:customer_user, :name => "Jose Carlos Araujo Neto", :email => "jose@test.com")
      @alessandro_user = Fabricate(:customer_user, :name => "Alessandro Marques Oliveira das Graças", :email => "alessandro@test.com")
      @mariagraca_user = Fabricate(:customer_user, :name => "Maria das Graças Xuxa Meneguel", :email => "xuxa@terra.com.br")
      @janacielle_user = Fabricate(:coach_user, :name => "Janacielle Morgana Organoide Maraclastos", :email => "maraclastos@krull.com.uk")
    end
    it "should list users in alphabetical order" do
      get "/admin/users.json"
      response.code.should == "200"
      JSON.parse(response.body).should == [
          { "id" => @alessandro_user.id.to_s, "name" => "Alessandro Marques Oliveira das Graças", "email" => "alessandro@test.com", "role" => I18n.t("models.role.customer") },
          { "id" => @bonovox_user.id.to_s, "name" => "Bonovox Magna Opus Dei", "email" => "pericles.silva@yahoo.com.br", "role" => I18n.t("models.role.coach") },
          { "id" => @coach_user.id.to_s, "name" => "Coach", "email" => "coach@test.com", "role" => I18n.t("models.role.coach") },
          { "id" => @janacielle_user.id.to_s, "name" => "Janacielle Morgana Organoide Maraclastos", "email" => "maraclastos@krull.com.uk", "role" => I18n.t("models.role.coach") },
          { "id" => @josecarlos_user.id.to_s, "name" => "Jose Carlos Araujo Neto", "email" => "jose@test.com", "role" => I18n.t("models.role.customer") },
          { "id" => @mariagraca_user.id.to_s, "name" => "Maria das Graças Xuxa Meneguel", "email" => "xuxa@terra.com.br", "role" => I18n.t("models.role.customer") }
        ]
    end    
  end
  describe "user show" do
    before do
      some_user = Fabricate(:customer_user, :name => "Adalmana de Mattos Mendes", :email => "mattos@spaces.org")
      @some_user_id = some_user.id.to_s
    end
    it "should display his/her public data" do
      get "/admin/users/"+@some_user_id+".json"
      response.code.should == "200"
      JSON.parse(response.body).should == {
        "name" => "Adalmana de Mattos Mendes",
        "email" => "mattos@spaces.org",
        "role" => I18n.t("models.role.customer")
      }
    end
  end
  after(:all) do
    User.all.destroy
  end
  describe "user messages" do
    before do
      @jptrainner_coach = Fabricate(:coach_user, :name => "João Paulo Cardodo Marchezi", :email => "jptrainner@email.com", :password => "12345678", :password_confirmation => "12345678")
      @jose_customer = Fabricate(:customer_user, :name => "José Alvarez")
      @naoveja_customer = Fabricate(:customer_user, :name => "Não Me Veja")
      @wagner_coach = Fabricate(:coach_user, :name => "Wagner Figueiredo")
      @msg1 = Fabricate( :customer_message, :from => @jose_customer,                                 :date => Time.new(2013,5,20, 9, 4,23), :content => "My second customer message")
      @msg2 = Fabricate( :coach_message,    :from => @jptrainner_coach,       :to => @jose_customer, :date => Time.new(2013,5,20,17,15,13), :content => "Shutup and do as I say")        
      @msg3 = Fabricate( :customer_message, :from => @jose_customer,                                 :date => Time.new(2013,5,22,15,20,20), :content => "My first customer message")
      @msg4 = Fabricate( :coach_message,    :from => @jptrainner_coach,       :to => @jose_customer, :date => Time.new(2013,5,24,11,10, 3), :content => "I just read it")
      @msg5 = Fabricate( :coach_message,    :from => @wagner_coach,          :to => @jose_customer,  :date => Time.new(2013,5,24,18,20,10), :content => "Follow JP's order")
      @msg6 = Fabricate( :customer_message, :from => @jose_customer,                                 :date => Time.new(2013,5,25,10, 0, 5), :content => "My other message")
      @msg7 = Fabricate( :customer_message, :from => @naoveja_customer,                              :date => Time.new(2013,5,26, 9, 0,23), :content => "You should not see this")      
      @msg8 = Fabricate( :coach_message,    :from => @jptrainner_coach,    :to => @naoveja_customer, :date => Time.new(2013,6,2,10,12,15),  :content => "Não me veja")
      post "/users/sign_in.json", { "user" => { "email" => "jptrainner@email.com", "password" => "12345678" } }
    end
    it "should list his conversations with coaches" do
      url = "/admin/users/#{@jose_customer.id}/messages.json"
      get url
      response.code.should == "200"
      json_object = JSON.parse(response.body)
        json_object.should == [
          { "from_id" => @jose_customer.id.to_s,    "from" => "José Alvarez",                "to" => "treinadores",  "to_id" => "0",                    "date" => "2013/05/20 09:04:23", "content" => "My second customer message", "position" => "left" },
          { "from_id" => @jptrainner_coach.id.to_s, "from" => "João Paulo Cardodo Marchezi", "to" => "José Alvarez", "to_id" => @jose_customer.id.to_s, "date" => "2013/05/20 17:15:13", "content" => "Shutup and do as I say", "position" => "right" },        
          { "from_id" => @jose_customer.id.to_s,    "from" => "José Alvarez",                "to" => "treinadores",  "to_id" => "0",                    "date" => "2013/05/22 15:20:20", "content" => "My first customer message", "position" => "left" },
          { "from_id" => @jptrainner_coach.id.to_s, "from" => "João Paulo Cardodo Marchezi", "to" => "José Alvarez", "to_id" => @jose_customer.id.to_s, "date" => "2013/05/24 11:10:03", "content" => "I just read it", "position" => "right" },
          { "from_id" => @wagner_coach.id.to_s,     "from" => "Wagner Figueiredo",           "to" => "José Alvarez", "to_id" => @jose_customer.id.to_s, "date" => "2013/05/24 18:20:10", "content" => "Follow JP's order", "position" => "right" },
          { "from_id" => @jose_customer.id.to_s,    "from" => "José Alvarez",                "to" => "treinadores",  "to_id" => "0",                    "date" => "2013/05/25 10:00:05", "content" => "My other message", "position" => "left" },
        ]
    end
  end
end

