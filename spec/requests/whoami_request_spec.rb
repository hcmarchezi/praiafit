# encoding: utf-8

require "spec_helper"

describe "Who Am I request" do
  context "when no user is authenticated" do
    before do
      get "/users/whoami.json"
      @json = JSON.parse(response.body)
    end
    it "should indicate authenticated as false" do
      @json.should eql({ "authenticated" => false })
    end
  end
  context "when customer is authenticated" do
    before do
      Fabricate(:customer_user, :name => "José Carlos da Silva", :email => "jose@email.com", :password => "12345678", :password_confirmation => "12345678")
      post "/users/sign_in.json", { "user" => { "email" => "jose@email.com", "password" => "12345678" } }
      get "/users/whoami.json"
      @json = JSON.parse(response.body)
    end
    it "should indicate authenticated as true and return his/her name, email and type as customer" do
      @json.should eql({
        "authenticated" => true,
        "name" => "José Carlos da Silva",
        "email" => "jose@email.com",
        "role" => "customer"
      })
    end
  end
  context "when coach is authenticated" do
    before do
      Fabricate(:coach_user, :name => "João Paulo Cardoso Marchezi", :email => "jptrainner@email.com", :password => "12345678", :password_confirmation => "12345678")
      post "/users/sign_in.json", { "user" => { "email" => "jptrainner@email.com", "password" => "12345678" } }
      get "/users/whoami.json"
      @json = JSON.parse(response.body)
    end
    it "should indicate authenticated as true and return his/her name, email and type as coach" do
      @json.should eql({
        "authenticated" => true,
        "name" => "João Paulo Cardoso Marchezi",
        "email" => "jptrainner@email.com",
        "role" => "coach"
      })
    end
  end
end
